﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using PublishingHelper.Forms;

namespace PublishingHelper
{
    internal static class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            RunProgram().GetAwaiter().GetResult();
        }

        private static async Task RunProgram()
        {
            //var scheduler = await new PublisherSchedulerFactory().GetScheduler();
            await Task.CompletedTask;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
            //Application.Run(new MainForm(scheduler));
        }
    }
}
