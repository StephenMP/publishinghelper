﻿namespace PublishingHelper.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.videoIdInput = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.commentInput = new System.Windows.Forms.TextBox();
            this.submitYoutubeComment = new System.Windows.Forms.Button();
            this.viewAllScheduledComments = new System.Windows.Forms.Button();
            this.publishAtInput = new System.Windows.Forms.DateTimePicker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnDeleteSuggestedGames = new System.Windows.Forms.Button();
            this.txtSuggestedGameIds = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnViewSuggestedGames = new System.Windows.Forms.Button();
            this.btnDisconnectAll = new System.Windows.Forms.Button();
            this.btnConnectAll = new System.Windows.Forms.Button();
            this.btnDisconnectSpwnLabels = new System.Windows.Forms.Button();
            this.btnDisconnectBot = new System.Windows.Forms.Button();
            this.btnConnectSpwnLabels = new System.Windows.Forms.Button();
            this.btnConnectBot = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "PublishingHelper is running";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Everything needed for scheduled publishing help";
            this.notifyIcon1.Visible = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Video ID";
            // 
            // videoIdInput
            // 
            this.videoIdInput.Location = new System.Drawing.Point(6, 19);
            this.videoIdInput.Name = "videoIdInput";
            this.videoIdInput.Size = new System.Drawing.Size(185, 20);
            this.videoIdInput.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(204, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Publish At";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Comment";
            // 
            // commentInput
            // 
            this.commentInput.Location = new System.Drawing.Point(6, 58);
            this.commentInput.Multiline = true;
            this.commentInput.Name = "commentInput";
            this.commentInput.Size = new System.Drawing.Size(391, 86);
            this.commentInput.TabIndex = 11;
            // 
            // submitYoutubeComment
            // 
            this.submitYoutubeComment.Location = new System.Drawing.Point(6, 150);
            this.submitYoutubeComment.Name = "submitYoutubeComment";
            this.submitYoutubeComment.Size = new System.Drawing.Size(185, 23);
            this.submitYoutubeComment.TabIndex = 12;
            this.submitYoutubeComment.Text = "Submit";
            this.submitYoutubeComment.UseVisualStyleBackColor = true;
            // 
            // viewAllScheduledComments
            // 
            this.viewAllScheduledComments.Location = new System.Drawing.Point(207, 150);
            this.viewAllScheduledComments.Name = "viewAllScheduledComments";
            this.viewAllScheduledComments.Size = new System.Drawing.Size(190, 23);
            this.viewAllScheduledComments.TabIndex = 23;
            this.viewAllScheduledComments.Text = "View All Scheduled Comments";
            this.viewAllScheduledComments.UseVisualStyleBackColor = true;
            // 
            // publishAtInput
            // 
            this.publishAtInput.CustomFormat = "MM/dd/yyyy hh:mm tt";
            this.publishAtInput.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.publishAtInput.Location = new System.Drawing.Point(207, 19);
            this.publishAtInput.Name = "publishAtInput";
            this.publishAtInput.Size = new System.Drawing.Size(190, 20);
            this.publishAtInput.TabIndex = 31;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(15, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(411, 205);
            this.tabControl1.TabIndex = 34;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.viewAllScheduledComments);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.videoIdInput);
            this.tabPage1.Controls.Add(this.publishAtInput);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.commentInput);
            this.tabPage1.Controls.Add(this.submitYoutubeComment);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(403, 179);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Youtube";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnDeleteSuggestedGames);
            this.tabPage3.Controls.Add(this.txtSuggestedGameIds);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.btnViewSuggestedGames);
            this.tabPage3.Controls.Add(this.btnDisconnectAll);
            this.tabPage3.Controls.Add(this.btnConnectAll);
            this.tabPage3.Controls.Add(this.btnDisconnectSpwnLabels);
            this.tabPage3.Controls.Add(this.btnDisconnectBot);
            this.tabPage3.Controls.Add(this.btnConnectSpwnLabels);
            this.tabPage3.Controls.Add(this.btnConnectBot);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(403, 179);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Streaming";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnDeleteSuggestedGames
            // 
            this.btnDeleteSuggestedGames.Location = new System.Drawing.Point(293, 138);
            this.btnDeleteSuggestedGames.Name = "btnDeleteSuggestedGames";
            this.btnDeleteSuggestedGames.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteSuggestedGames.TabIndex = 10;
            this.btnDeleteSuggestedGames.Text = "Delete";
            this.btnDeleteSuggestedGames.UseVisualStyleBackColor = true;
            this.btnDeleteSuggestedGames.Click += new System.EventHandler(this.DeleteSuggestedGames_Click);
            // 
            // txtSuggestedGameIds
            // 
            this.txtSuggestedGameIds.Location = new System.Drawing.Point(7, 138);
            this.txtSuggestedGameIds.Name = "txtSuggestedGameIds";
            this.txtSuggestedGameIds.Size = new System.Drawing.Size(279, 20);
            this.txtSuggestedGameIds.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Delete Suggestion By Comma Seperated Ids";
            // 
            // btnViewSuggestedGames
            // 
            this.btnViewSuggestedGames.Location = new System.Drawing.Point(292, 4);
            this.btnViewSuggestedGames.Name = "btnViewSuggestedGames";
            this.btnViewSuggestedGames.Size = new System.Drawing.Size(95, 110);
            this.btnViewSuggestedGames.TabIndex = 7;
            this.btnViewSuggestedGames.Text = "View Suggested Games";
            this.btnViewSuggestedGames.UseVisualStyleBackColor = true;
            this.btnViewSuggestedGames.Click += new System.EventHandler(this.ViewSuggestedGames_Click);
            // 
            // btnDisconnectAll
            // 
            this.btnDisconnectAll.Location = new System.Drawing.Point(198, 64);
            this.btnDisconnectAll.Name = "btnDisconnectAll";
            this.btnDisconnectAll.Size = new System.Drawing.Size(87, 50);
            this.btnDisconnectAll.TabIndex = 6;
            this.btnDisconnectAll.Text = "Disconnect All";
            this.btnDisconnectAll.UseVisualStyleBackColor = true;
            this.btnDisconnectAll.Click += new System.EventHandler(this.DisconnectAll_Click);
            // 
            // btnConnectAll
            // 
            this.btnConnectAll.Location = new System.Drawing.Point(198, 4);
            this.btnConnectAll.Name = "btnConnectAll";
            this.btnConnectAll.Size = new System.Drawing.Size(88, 53);
            this.btnConnectAll.TabIndex = 5;
            this.btnConnectAll.Text = "Connect All";
            this.btnConnectAll.UseVisualStyleBackColor = true;
            this.btnConnectAll.Click += new System.EventHandler(this.ConnectAll_Click);
            // 
            // btnDisconnectSpwnLabels
            // 
            this.btnDisconnectSpwnLabels.Location = new System.Drawing.Point(100, 64);
            this.btnDisconnectSpwnLabels.Name = "btnDisconnectSpwnLabels";
            this.btnDisconnectSpwnLabels.Size = new System.Drawing.Size(93, 50);
            this.btnDisconnectSpwnLabels.TabIndex = 4;
            this.btnDisconnectSpwnLabels.Text = "Disconnect Labels";
            this.btnDisconnectSpwnLabels.UseVisualStyleBackColor = true;
            this.btnDisconnectSpwnLabels.Click += new System.EventHandler(this.DisconnectSpwnLabels_Click);
            // 
            // btnDisconnectBot
            // 
            this.btnDisconnectBot.Location = new System.Drawing.Point(4, 64);
            this.btnDisconnectBot.Name = "btnDisconnectBot";
            this.btnDisconnectBot.Size = new System.Drawing.Size(94, 50);
            this.btnDisconnectBot.TabIndex = 3;
            this.btnDisconnectBot.Text = "Disconnect Bot";
            this.btnDisconnectBot.UseVisualStyleBackColor = true;
            this.btnDisconnectBot.Click += new System.EventHandler(this.DisconnectBot_Click);
            // 
            // btnConnectSpwnLabels
            // 
            this.btnConnectSpwnLabels.Location = new System.Drawing.Point(100, 4);
            this.btnConnectSpwnLabels.Name = "btnConnectSpwnLabels";
            this.btnConnectSpwnLabels.Size = new System.Drawing.Size(93, 53);
            this.btnConnectSpwnLabels.TabIndex = 1;
            this.btnConnectSpwnLabels.Text = "Connect Labels";
            this.btnConnectSpwnLabels.UseVisualStyleBackColor = true;
            this.btnConnectSpwnLabels.Click += new System.EventHandler(this.ConnectSpwnLabels_Click);
            // 
            // btnConnectBot
            // 
            this.btnConnectBot.Location = new System.Drawing.Point(4, 4);
            this.btnConnectBot.Name = "btnConnectBot";
            this.btnConnectBot.Size = new System.Drawing.Size(93, 53);
            this.btnConnectBot.TabIndex = 0;
            this.btnConnectBot.Text = "Connect Bot";
            this.btnConnectBot.UseVisualStyleBackColor = true;
            this.btnConnectBot.Click += new System.EventHandler(this.ConnectBot_Clicked);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 220);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(25, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Log";
            // 
            // listBoxLog
            // 
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(15, 236);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.Size = new System.Drawing.Size(407, 108);
            this.listBoxLog.TabIndex = 35;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 362);
            this.Controls.Add(this.listBoxLog);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label12);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "PublishingHelper";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox videoIdInput;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox commentInput;
        private System.Windows.Forms.Button submitYoutubeComment;
        private System.Windows.Forms.Button viewAllScheduledComments;
        private System.Windows.Forms.DateTimePicker publishAtInput;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnConnectSpwnLabels;
        private System.Windows.Forms.Button btnConnectBot;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.Button btnDisconnectBot;
        private System.Windows.Forms.Button btnDisconnectSpwnLabels;
        private System.Windows.Forms.Button btnDisconnectAll;
        private System.Windows.Forms.Button btnConnectAll;
        private System.Windows.Forms.Button btnViewSuggestedGames;
        private System.Windows.Forms.Button btnDeleteSuggestedGames;
        private System.Windows.Forms.TextBox txtSuggestedGameIds;
        private System.Windows.Forms.Label label1;
    }
}

