﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PublishingHelper.Forms.Components
{
    public sealed class ListBoxLog : IDisposable
    {
        private readonly int maxEntriesInListBox;
        private readonly string messageFormat;
        private bool canAdd;
        private bool disposed;
        private readonly ListBox listBox;

        public ListBoxLog(ListBox listBox, string messageFormat, int maxLinesInListbox)
        {
            this.listBox = listBox;
            this.messageFormat = messageFormat;
            this.maxEntriesInListBox = maxLinesInListbox;

            this.canAdd = listBox.IsHandleCreated;

            this.listBox.SelectionMode = SelectionMode.MultiExtended;
            this.listBox.HandleCreated += OnHandleCreated;
            this.listBox.HandleDestroyed += OnHandleDestroyed;
            this.listBox.DrawItem += DrawItemHandler;
            this.listBox.DrawMode = DrawMode.OwnerDrawFixed;
        }

        private delegate void AddALogEntryDelegate(object item);

        public void Dispose()
        {
            if (!this.disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
                this.disposed = true;
            }
        }

        public void Log(string message)
        {
            if (this.canAdd)
            {
                this.listBox.BeginInvoke(new AddALogEntryDelegate(AddALogEntry), new LogEvent(message));
            }
        }

        private void AddALogEntry(object item)
        {
            this.listBox.Items.Insert(0, item);

            if (this.listBox.Items.Count > this.maxEntriesInListBox)
            {
                this.listBox.Items.RemoveAt(0);
            }
        }

        private void Dispose(bool disposing)
        {
            if (this.listBox != null)
            {
                this.canAdd = false;

                this.listBox.HandleCreated -= OnHandleCreated;
                this.listBox.HandleCreated -= OnHandleDestroyed;
                this.listBox.DrawItem -= DrawItemHandler;

                this.listBox.ContextMenu = null;

                this.listBox.Items.Clear();
                this.listBox.DrawMode = DrawMode.Normal;
                this.listBox.Dispose();
            }
        }

        private void DrawItemHandler(object sender, DrawItemEventArgs e)
        {
            if (e.Index >= 0)
            {
                e.DrawBackground();
                e.DrawFocusRectangle();

                // SafeGuard against wrong configuration of list box
                if (!(((ListBox)sender).Items[e.Index] is LogEvent logEvent))
                {
                    logEvent = new LogEvent(((ListBox)sender).Items[e.Index].ToString());
                }

                e.Graphics.DrawString(FormatALogEventMessage(logEvent, this.messageFormat), new Font("Lucida Console", 8.25f, FontStyle.Regular), new SolidBrush(Color.Green), e.Bounds);
            }
        }

        private string FormatALogEventMessage(LogEvent logEvent, string messageFormat)
        {
            string message = logEvent.Message;
            if (message == null) { message = "<NULL>"; }
            return string.Format(messageFormat, logEvent.EventTime.ToString("HH:mm:ss"), message);
        }

        private void OnHandleCreated(object sender, EventArgs e)
        {
            this.canAdd = true;
        }

        private void OnHandleDestroyed(object sender, EventArgs e)
        {
            this.canAdd = false;
        }

        ~ListBoxLog()
        {
            if (!this.disposed)
            {
                Dispose(false);
                this.disposed = true;
            }
        }

        private class LogEvent
        {
            public readonly DateTime EventTime;
            public readonly string Message;

            public LogEvent(string message)
            {
                EventTime = DateTime.Now;
                Message = message;
            }
        }
    }
}