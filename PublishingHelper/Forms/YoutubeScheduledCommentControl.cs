﻿using System;
using System.Windows.Forms;
using PublishingHelper.Forms.Components;
using PublishingHelper.Repository.Entities;
using PublishingHelper.Service;

namespace PublishingHelper.Forms
{
    public partial class YoutubeScheduledCommentControl : UserControl
    {
        private Label horizontalLine;
        private YoutubeScheduledCommentEntity scheduledComment;

        public YoutubeScheduledCommentControl(YoutubeScheduledCommentEntity scheduledComment, Label horizontalLine)
        {
            InitializeComponent();
            this.id.Text = scheduledComment.Id.ToString();
            this.title.Text = scheduledComment.VideoTitle;
            this.videoId.Text = scheduledComment.VideoId;
            this.comment.Text = scheduledComment.Comment;
            this.publishAt.Value = scheduledComment.PublishAt;
            this.scheduledComment = scheduledComment;
            this.horizontalLine = horizontalLine;

            this.btnUpdate.Click += UpdateScheduledComment;
            this.btnDelete.Click += DeleteScheduledComment;
        }

        private void DeleteScheduledComment(object sender, EventArgs args)
        {
            var youtubeService = new YoutubeService();
            youtubeService.DeleteScheduledComment(this.scheduledComment.VideoId);

            this.Dispose();
            this.horizontalLine.Dispose();

            FlexibleMessageBox.Show("Scheduled Comment Deleted");
        }

        private void UpdateScheduledComment(object sender, EventArgs args)
        {
            var youtubeService = new YoutubeService();
            this.scheduledComment.Comment = this.comment.Text;
            this.scheduledComment.PublishAt = this.publishAt.Value;
            this.scheduledComment = youtubeService.UpdateScheduledComment(this.scheduledComment);

            FlexibleMessageBox.Show("Scheduled Comment Updated");
        }
    }
}
