﻿using System.Collections.Generic;
using System.Windows.Forms;
using PublishingHelper.Repository.Entities;

namespace PublishingHelper.Forms
{
    public partial class YoutubeScheduledCommentViewForm : Form
    {
        public YoutubeScheduledCommentViewForm(IEnumerable<YoutubeScheduledCommentEntity> scheduledComments)
        {
            InitializeComponent();
            BuildUserControls(scheduledComments);
        }

        private void BuildUserControls(IEnumerable<YoutubeScheduledCommentEntity> scheduledComments)
        {
            foreach (var comment in scheduledComments)
            {
                var horizontalLine = new Label();
                horizontalLine.Text = "";
                horizontalLine.BorderStyle = BorderStyle.Fixed3D;
                horizontalLine.AutoSize = false;
                horizontalLine.Height = 2;
                horizontalLine.Width = this.Width;

                var lStyle = new RowStyle(SizeType.AutoSize);
                this.controlPanel.RowStyles.Add(lStyle);
                this.controlPanel.Controls.Add(horizontalLine, 0, this.controlPanel.RowCount++);

                var control = new YoutubeScheduledCommentControl(comment, horizontalLine);
                var style = new RowStyle(SizeType.AutoSize);
                this.controlPanel.RowStyles.Add(style);

                this.controlPanel.Controls.Add(control, 0, this.controlPanel.RowCount++);
            }
        }
    }
}
