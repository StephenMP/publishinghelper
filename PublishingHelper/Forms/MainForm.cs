﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Discord;
using PublishingHelper.ChatBot;
using PublishingHelper.ChatBot.IoC;
using PublishingHelper.Discord;
using PublishingHelper.Forms.Components;
using PublishingHelper.Properties;
using PublishingHelper.Repository;
using PublishingHelper.Service;
using PublishingHelper.StreamLabs;

namespace PublishingHelper.Forms
{
    public partial class MainForm : Form
    {
        private readonly ListBoxLog log;
        //private readonly IScheduler scheduler;
        private ChatBotClient chatBot;
        private bool isBotConnected;
        private bool isSpwnLabelsConnected;
        private SpwnLabels spwnLabels;
        private readonly IList<IMessage> goLiveMessages;

        //internal MainForm(IScheduler scheduler)
        internal MainForm()
        {
            this.InitializeComponent();

            this.log = new ListBoxLog(this.listBoxLog, "{0} > {1}", 20);
            this.goLiveMessages = new List<IMessage>();

            //this.scheduler = scheduler;
            //this.scheduler.ListenerManager.AddJobListener(new JobLogListener(this.Log), GroupMatcher<JobKey>.AnyGroup());
            //this.scheduler.Start();
        }

        public void Log(string message)
        {
            this.log.Log(message);
        }

        private void ConnectBot_Clicked(object sender, EventArgs e)
        {
            if (!this.isBotConnected)
            {
                this.chatBot = new ChatBotClient(Settings.Default.TwitchOAuthToken, this.Log);
                this.isBotConnected = true;
            }
        }

        private void ConnectSpwnLabels_Click(object sender, EventArgs e)
        {
            if (!this.isSpwnLabelsConnected)
            {
                this.spwnLabels = new SpwnLabels(this.Log);
                this.isSpwnLabelsConnected = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Resize += new EventHandler(Form1_Resize);
            this.notifyIcon1.DoubleClick += new EventHandler(NotifyIcon1_DoubleClick);
            this.submitYoutubeComment.Click += new EventHandler(SubmitYoutubeComment);
            this.viewAllScheduledComments.Click += new EventHandler(ViewAllScheduledComments);
            this.FormClosing += new FormClosingEventHandler(Shutdown);
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Hide();
                this.notifyIcon1.Visible = true;
            }
        }

        private void NotifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.notifyIcon1.Visible = false;
        }

        private void Shutdown(object sender, EventArgs e)
        {
            this.spwnLabels?.Dispose();
            this.chatBot?.Dispose();
            //this.scheduler?.Shutdown(true).Wait();
            this.log?.Dispose();
        }

        private async void SubmitYoutubeComment(object sender, EventArgs e)
        {
            var videoId = this.videoIdInput.Text;
            var publishAt = this.publishAtInput.Value;
            var comment = this.commentInput.Text;
            var youtubeService = ServiceLocator.Current.Resolve<IYoutubeService>();
            var video = await youtubeService.GetVideoByIdAsync(videoId, "snippet");

            if (video != null)
            {
                youtubeService.CreateScheduleComment(videoId, publishAt, comment, video.Snippet.Title);
                this.videoIdInput.Clear();
                this.commentInput.Clear();

                FlexibleMessageBox.Show("Comment Scheduled");
            }
            else
            {
                FlexibleMessageBox.Show("Video does not exist");
            }
        }

        private void ViewAllScheduledComments(object sender, EventArgs e)
        {
            var youtubeService = ServiceLocator.Current.Resolve<IYoutubeService>();
            var scheduledComments = youtubeService.ReadAllScheduledComments();
            var youtubeScheduledCommentViewForm = new YoutubeScheduledCommentViewForm(scheduledComments);
            youtubeScheduledCommentViewForm.ShowDialog(this);
        }

        private async void AnnounceOnDiscord_Click(object sender, EventArgs e)
        {
            var helper = await DiscordHelper.GetCurrentAsync();
            var message = await helper.PublishGoLive("I just went live!\nYouTube: https://goo.gl/cnNgCk\nMixer: https://goo.gl/q54KjD\nTwitch: https://goo.gl/k5T5mH");
            this.goLiveMessages.Add(message);
            this.Log("Announced gone live on Discord");
        }

        private void ConnectAll_Click(object sender, EventArgs e)
        {
            this.ConnectSpwnLabels_Click(sender, e);
            this.ConnectBot_Clicked(sender, e);
            this.AnnounceOnDiscord_Click(sender, e);
        }

        private void DeleteSuggestedGames_Click(object sender, EventArgs e)
        {
            var gameIds = this.txtSuggestedGameIds.Text;
            if (!string.IsNullOrWhiteSpace(gameIds))
            {
                var gameIdsArray = gameIds.Split(',');
                var suggestedGamesRepository = new SuggestedGameRepository();
                foreach (var gameId in gameIdsArray)
                {
                    var id = Convert.ToInt32(gameId);
                    suggestedGamesRepository.Delete(id);
                }

                FlexibleMessageBox.Show("All game suggestions have been removed!");
                this.txtSuggestedGameIds.Clear();
            }
        }

        private async void DisconnectAll_Click(object sender, EventArgs e)
        {
            this.DisconnectSpwnLabels_Click(sender, e);
            this.DisconnectBot_Click(sender, e);

            if (this.goLiveMessages.Count > 0)
            {
                var discordHelper = await DiscordHelper.GetCurrentAsync();
                await discordHelper.DeleteMessagesAsync(this.goLiveMessages);
                this.goLiveMessages.Clear();

                this.Log("Deleted live announcement on Discord");
            }
        }

        private void DisconnectBot_Click(object sender, EventArgs e)
        {
            if (this.isBotConnected)
            {
                this.chatBot?.Dispose();
                this.chatBot = null;

                this.isBotConnected = false;
            }
        }

        private void DisconnectSpwnLabels_Click(object sender, EventArgs e)
        {
            if (this.isSpwnLabelsConnected)
            {
                this.spwnLabels?.Dispose();
                this.spwnLabels = null;

                this.isSpwnLabelsConnected = false;
            }
        }

        private void ViewSuggestedGames_Click(object sender, EventArgs e)
        {
            var suggestedGameRepository = new SuggestedGameRepository();
            var suggestedGames = suggestedGameRepository.ReadAll();
            var stringBuilder = new StringBuilder();
            foreach (var game in suggestedGames)
            {
                stringBuilder.AppendLine(game.ToString()).AppendLine();
            }

            FlexibleMessageBox.Show(stringBuilder.ToString());
        }
    }
}
