﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Rest;

namespace PublishingHelper.Discord
{
    public class DiscordHelper
    {
        private static DiscordHelper _current;
        private DiscordRestClient client;

        public DiscordHelper()
        {
            this.client = new DiscordRestClient();
        }

        public static async Task<DiscordHelper> GetCurrentAsync()
        {
            if (_current == null)
            {
                _current = new DiscordHelper();
                await _current.LoginAsync();
            }

            return _current;
        }

        public async Task LoginAsync()
        {
            await this.client.LoginAsync(TokenType.Bot, "NDA4Nzc3NTAwMDU1OTYxNjAz.DVU_WQ.SKQfLtn2al7POGvsQtCrWDBpCX0");
        }

        public async Task<RestUserMessage> PublishGoLive(string goLiveMessage)
        {
            if (ValidateConnection())
            {
                var spwnsHouse = await this.client.GetGuildAsync(269890563887595530);
                var publicChatChannel = await spwnsHouse.GetTextChannelAsync(269890563887595530);
                return await publicChatChannel.SendMessageAsync($"Hey @everyone! {goLiveMessage}");
            }

            return null;
        }

        public async Task PublishYouTubeVideoMessageAsync(string videoId, string comment)
        {
            if (ValidateConnection())
            {
                var shareLink = $"https://youtu.be/{videoId}";
                var spwnsHouse = await this.client.GetGuildAsync(269890563887595530);
                var customContentChannel = await spwnsHouse.GetTextChannelAsync(399750345422798859);
                await customContentChannel.SendMessageAsync($"Hey @everyone! New video just went live!\n{comment}\n{shareLink}");
            }
        }

        public async Task DeleteMessagesAsync(IEnumerable<IMessage> messages)
        {
            if (ValidateConnection())
            {
                var spwnsHouse = await this.client.GetGuildAsync(269890563887595530);
                var publicChatChannel = await spwnsHouse.GetTextChannelAsync(269890563887595530);
                await publicChatChannel.DeleteMessagesAsync(messages);
            }
        }

        private bool ValidateConnection()
        {
            var retryCount = 0;

            while (this.client.LoginState != LoginState.LoggedIn)
            {
                retryCount++;
                if (retryCount == 20)
                {
                    return false;
                }

                Thread.Sleep(100);
            }

            return true;
        }
    }
}
