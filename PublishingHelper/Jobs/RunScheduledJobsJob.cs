﻿using System;
using System.Linq;
using System.Threading.Tasks;
using PublishingHelper.Discord;
using PublishingHelper.Service;
using Quartz;

namespace PublishingHelper.Jobs
{
    public class RunScheduledJobsJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            var youtubeService = new YoutubeService();

            foreach (var scheduledComment in youtubeService.ReadAllScheduledComments().Where(sc => sc.PublishAt <= DateTime.Now))
            {
                await youtubeService.PublishScheduledCommentAsync(scheduledComment.VideoId, scheduledComment.Comment);
                youtubeService.DeleteScheduledComment(scheduledComment.VideoId);

                var discordHelper = await DiscordHelper.GetCurrentAsync();
                await discordHelper.PublishYouTubeVideoMessageAsync(scheduledComment.VideoId, scheduledComment.Comment);
            }
        }
    }
}
