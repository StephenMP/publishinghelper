﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Quartz;

namespace PublishingHelper.Jobs
{
    public class JobLogListener : IJobListener
    {
        private Action<string> Log;

        public string Name => this.GetType().Name;

        public JobLogListener(Action<string> loggerDelegate)
        {
            this.Log = loggerDelegate;
        }

        public Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            this.Log($"{context.JobDetail.Key.Name} was vetoed");
            return Task.CompletedTask;
        }

        public Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            this.Log($"Starting {context.JobDetail.Key.Name}");
            return Task.CompletedTask;
        }

        public Task JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException, CancellationToken cancellationToken = default(CancellationToken))
        {
            this.Log($"Finished {context.JobDetail.Key.Name}");
            return Task.CompletedTask;
        }
    }
}
