﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using PublishingHelper.ChatBot.IoC;
using PublishingHelper.Repository.Repository;

namespace PublishingHelper.Youtube
{
    internal interface IYoutubeAuthenticationService
    {
        Task<UserCredential> GetUserCredentialAsync();
    }

    internal class YoutubeAuthenticationService : IYoutubeAuthenticationService
    {
        private DateTime lastAuthorized;
        private UserCredential user;

        public async Task<UserCredential> GetUserCredentialAsync()
        {
            var clientIdPath = @"C:\Users\Stephen\Desktop\PublishingHelper\client_id.json";
            var youtubeDeveloperCredentialRepository = new YouTubeDeveloperCredentialRepository();
            if (this.user == null || this.lastAuthorized.AddHours(24) <= DateTime.Now)
            {
                this.lastAuthorized = DateTime.Now;
                using (var stream = new FileStream(clientIdPath, FileMode.Open))
                {
                    try
                    {
                        var loadedSecrets = GoogleClientSecrets.Load(stream);
                        var clientSecrets = loadedSecrets.Secrets;
                        var scopes = new[] { YouTubeService.Scope.YoutubeForceSsl };
                        var user = "itsspwn@gmail.com";
                        var dataStore = ServiceLocator.Current.Resolve<IDataStore>();
                        this.user = await GoogleWebAuthorizationBroker.AuthorizeAsync(clientSecrets, scopes, user, CancellationToken.None, dataStore);
                    }

                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }

            return this.user;
        }
    }
}
