﻿using System.Linq;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;

namespace PublishingHelper.Youtube
{
    internal interface IYoutubeClient
    {
        Video GetVideoById(string videoId, params string[] parts);
        void LikeComment(Comment comment);
        void LikeVideo(string videoId);
        CommentThread PostCommentToVideo(string videoId, string comment);
    }

    internal class YoutubeClient : IYoutubeClient
    {
        private readonly YouTubeService youtubeService;

        public YoutubeClient(UserCredential credential)
        {
            var initializer = new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = this.GetType().ToString()
            };

            this.youtubeService = new YouTubeService(initializer);
        }

        public Video GetVideoById(string videoId, params string[] parts)
        {
            var videoParts = (parts != null && parts.Length > 0) ? string.Join(",", parts) : "status";
            var videoRequest = this.youtubeService.Videos.List(videoParts);
            videoRequest.Id = videoId;

            var videoResponse = videoRequest.Execute();
            return videoResponse.Items.FirstOrDefault();
        }

        public void LikeComment(Comment comment)
        {
            comment.Snippet.ViewerRating = "like";
            var commentRequest = this.youtubeService.Comments.Update(comment, "snippet");

            commentRequest.Execute();
        }

        public void LikeVideo(string videoId)
        {
            var currentRatingRequest = this.youtubeService.Videos.GetRating(videoId);
            var currentRatingResponse = currentRatingRequest.Execute();
            var currentRating = currentRatingResponse.Items.FirstOrDefault();

            if (currentRating != null && currentRating.Rating != "like")
            {
                var likeRequest = this.youtubeService.Videos.Rate(videoId, VideosResource.RateRequest.RatingEnum.Like);
                likeRequest.Execute();
            }
        }

        public CommentThread PostCommentToVideo(string videoId, string comment)
        {
            var commentSnippet = new CommentSnippet { VideoId = videoId, TextOriginal = comment };
            var topLevelComment = new Comment { Snippet = commentSnippet };
            var commentThreadSnippet = new CommentThreadSnippet { TopLevelComment = topLevelComment, VideoId = videoId };
            var commentThread = new CommentThread { Snippet = commentThreadSnippet };
            var commentRequest = this.youtubeService.CommentThreads.Insert(commentThread, "snippet");

            return commentRequest.Execute();
        }
    }
}
