﻿using System;
using System.Collections.Generic;
using System.Linq;
using Quartz;

namespace PublishingHelper.Factories
{
    internal class YearExpressionBuilder
    {
        private readonly YearField yearField;
        private readonly CronExpressionBuilder cronExpressionBuilder;

        public YearExpressionBuilder(CronExpressionBuilder cronExpressionBuilder)
        {
            this.yearField = new YearField();
            this.cronExpressionBuilder = cronExpressionBuilder;
        }

        public CronExpressionBuilder SpecificYears(params int[] seconds)
        {
            this.yearField.SpecificValues(seconds);
            return this.cronExpressionBuilder;
        }

        public CronExpressionBuilder RangeOfYears(int from, int to)
        {
            this.yearField.RangeOfValues(from, to);
            return this.cronExpressionBuilder;
        }

        public CronExpressionBuilder AllYears()
        {
            this.yearField.AllValues();
            return this.cronExpressionBuilder;
        }

        public CronExpressionBuilder RunInYearsIncrements(int startingValue, int increment)
        {
            this.yearField.RunInIncrements(startingValue, increment);
            return this.cronExpressionBuilder;
        }

        public CronExpressionBuilder RunEveryXYears(int interval)
        {
            this.yearField.RunEveryXMinutes(interval);
            return this.cronExpressionBuilder;
        }

        public string BuildCronExpression() => this.yearField.CronExpression;
    }

    internal class DayOfTheWeekExpressionBuilder
    {
        private readonly DayOfWeekField dayOfWeekField;
        private readonly YearExpressionBuilder yearExpressionBuilder;

        public DayOfTheWeekExpressionBuilder(CronExpressionBuilder cronExpressionBuilder)
        {
            this.dayOfWeekField = new DayOfWeekField();
            this.yearExpressionBuilder = new YearExpressionBuilder(cronExpressionBuilder);
        }

        public YearExpressionBuilder SpecificDaysOfTheWeek(params int[] seconds)
        {
            this.dayOfWeekField.SpecificValues(seconds);
            return this.yearExpressionBuilder;
        }

        public YearExpressionBuilder RangeOfDaysOfTheWeek(int from, int to)
        {
            this.dayOfWeekField.RangeOfValues(from, to);
            return this.yearExpressionBuilder;
        }

        public YearExpressionBuilder AllDaysOfTheWeek()
        {
            this.dayOfWeekField.AllValues();
            return this.yearExpressionBuilder;
        }

        public YearExpressionBuilder RunInDaysOfTheWeekIncrements(int startingValue, int increment)
        {
            this.dayOfWeekField.RunInIncrements(startingValue, increment);
            return this.yearExpressionBuilder;
        }

        public YearExpressionBuilder NoSpecificDaysOfTheWeek()
        {
            this.dayOfWeekField.NoSpecificValue();
            return this.yearExpressionBuilder;
        }

        public YearExpressionBuilder LastDaysOfTheWeek()
        {
            this.dayOfWeekField.Last();
            return this.yearExpressionBuilder;
        }

        public YearExpressionBuilder NthDayOfMonth(int dayOfWeek, int weekNumber)
        {
            this.dayOfWeekField.NthDayOfMonth(dayOfWeek, weekNumber);
            return this.yearExpressionBuilder;
        }

        public string BuildCronExpression() => $"{this.dayOfWeekField.CronExpression} {this.yearExpressionBuilder.BuildCronExpression()}";
    }

    internal class MonthExpressionBuilder
    {
        private readonly MonthField monthField;
        private readonly DayOfTheWeekExpressionBuilder dayOfTheWeekExpressionBuilder;

        public MonthExpressionBuilder(CronExpressionBuilder cronExpressionBuilder)
        {
            this.monthField = new MonthField();
            this.dayOfTheWeekExpressionBuilder = new DayOfTheWeekExpressionBuilder(cronExpressionBuilder);
        }

        public DayOfTheWeekExpressionBuilder SpecificMonths(params int[] seconds)
        {
            this.monthField.SpecificValues(seconds);
            return this.dayOfTheWeekExpressionBuilder;
        }

        public DayOfTheWeekExpressionBuilder RangeOfMonths(int from, int to)
        {
            this.monthField.RangeOfValues(from, to);
            return this.dayOfTheWeekExpressionBuilder;
        }

        public DayOfTheWeekExpressionBuilder AllMonths()
        {
            this.monthField.AllValues();
            return this.dayOfTheWeekExpressionBuilder;
        }

        public DayOfTheWeekExpressionBuilder RunInMonthsIncrements(int startingValue, int increment)
        {
            this.monthField.RunInIncrements(startingValue, increment);
            return this.dayOfTheWeekExpressionBuilder;
        }

        public DayOfTheWeekExpressionBuilder RunEveryXMonths(int interval)
        {
            this.monthField.RunEveryXMinutes(interval);
            return this.dayOfTheWeekExpressionBuilder;
        }

        public string BuildCronExpression() => $"{this.monthField.CronExpression} {this.dayOfTheWeekExpressionBuilder.BuildCronExpression()}";
    }

    internal class DayOfTheMonthExpressionBuilder
    {
        private readonly DayOfMonthField dayOfMonthField;
        private readonly MonthExpressionBuilder monthExpressionBuilder;

        public DayOfTheMonthExpressionBuilder(CronExpressionBuilder cronExpressionBuilder)
        {
            this.dayOfMonthField = new DayOfMonthField();
            this.monthExpressionBuilder = new MonthExpressionBuilder(cronExpressionBuilder);
        }

        public MonthExpressionBuilder SpecificDaysOfTheMonth(params int[] seconds)
        {
            this.dayOfMonthField.SpecificValues(seconds);
            return this.monthExpressionBuilder;
        }

        public MonthExpressionBuilder RangeOfDaysOfTheMonth(int from, int to)
        {
            this.dayOfMonthField.RangeOfValues(from, to);
            return this.monthExpressionBuilder;
        }

        public MonthExpressionBuilder AllDaysOfTheMonth()
        {
            this.dayOfMonthField.AllValues();
            return this.monthExpressionBuilder;
        }

        public MonthExpressionBuilder RunInDaysOfTheMonthIncrements(int startingValue, int increment)
        {
            this.dayOfMonthField.RunInIncrements(startingValue, increment);
            return this.monthExpressionBuilder;
        }

        public MonthExpressionBuilder NoSpecificDaysOfTheMonth()
        {
            this.dayOfMonthField.NoSpecificValue();
            return this.monthExpressionBuilder;
        }

        public MonthExpressionBuilder LastDayOfTheMonth()
        {
            this.dayOfMonthField.Last();
            return this.monthExpressionBuilder;
        }

        public MonthExpressionBuilder WeekdaysOnly()
        {
            this.dayOfMonthField.WeekdaysOnly();
            return this.monthExpressionBuilder;
        }

        public string BuildCronExpression() => $"{this.dayOfMonthField.CronExpression} {this.monthExpressionBuilder.BuildCronExpression()}";
    }

    internal class HoursExpressionBuilder
    {
        private readonly HoursField hoursField;
        private readonly DayOfTheMonthExpressionBuilder dayOfTheMonthExpressionBuilder;

        public HoursExpressionBuilder(CronExpressionBuilder cronExpressionBuilder)
        {
            this.hoursField = new HoursField();
            this.dayOfTheMonthExpressionBuilder = new DayOfTheMonthExpressionBuilder(cronExpressionBuilder);
        }

        public DayOfTheMonthExpressionBuilder SpecificHours(params int[] seconds)
        {
            this.hoursField.SpecificValues(seconds);
            return this.dayOfTheMonthExpressionBuilder;
        }

        public DayOfTheMonthExpressionBuilder RangeOfHours(int from, int to)
        {
            this.hoursField.RangeOfValues(from, to);
            return this.dayOfTheMonthExpressionBuilder;
        }

        public DayOfTheMonthExpressionBuilder AllHours()
        {
            this.hoursField.AllValues();
            return this.dayOfTheMonthExpressionBuilder;
        }

        public DayOfTheMonthExpressionBuilder RunInHourIncrements(int startingValue, int increment)
        {
            this.hoursField.RunInIncrements(startingValue, increment);
            return this.dayOfTheMonthExpressionBuilder;
        }

        public DayOfTheMonthExpressionBuilder RunEveryXHours(int interval)
        {
            this.hoursField.RunEveryXMinutes(interval);
            return this.dayOfTheMonthExpressionBuilder;
        }

        public string BuildCronExpression() => $"{this.hoursField.CronExpression} {this.dayOfTheMonthExpressionBuilder.BuildCronExpression()}";
    }

    internal class MinutesExpressionBuilder
    {
        private readonly MinutesField minutesField;
        private readonly HoursExpressionBuilder hoursExpressionBuilder;

        public MinutesExpressionBuilder(CronExpressionBuilder cronExpressionBuilder)
        {
            this.minutesField = new MinutesField();
            this.hoursExpressionBuilder = new HoursExpressionBuilder(cronExpressionBuilder);
        }

        public HoursExpressionBuilder SpecificMinutes(params int[] seconds)
        {
            this.minutesField.SpecificValues(seconds);
            return this.hoursExpressionBuilder;
        }

        public HoursExpressionBuilder RangeOfMinutes(int from, int to)
        {
            this.minutesField.RangeOfValues(from, to);
            return this.hoursExpressionBuilder;
        }

        public HoursExpressionBuilder AllMinutes()
        {
            this.minutesField.AllValues();
            return this.hoursExpressionBuilder;
        }

        public HoursExpressionBuilder RunInMinuteIncrements(int startingValue, int increment)
        {
            this.minutesField.RunInIncrements(startingValue, increment);
            return this.hoursExpressionBuilder;
        }

        public HoursExpressionBuilder RunEveryXMinutes(int interval)
        {
            this.minutesField.RunEveryXMinutes(interval);
            return this.hoursExpressionBuilder;
        }

        public string BuildCronExpression() => $"{this.minutesField.CronExpression} {this.hoursExpressionBuilder.BuildCronExpression()}";
    }

    internal class CronExpressionBuilder
    {
        public static CronExpressionBuilder NewExpression() => new CronExpressionBuilder();

        private readonly SecondsField secondsField;
        private readonly MinutesExpressionBuilder minutesExpressionBuilder;

        public CronExpressionBuilder()
        {
            this.secondsField = new SecondsField();
            this.minutesExpressionBuilder = new MinutesExpressionBuilder(this);
        }

        public MinutesExpressionBuilder SpecificSeconds(params int[] seconds)
        {
            this.secondsField.SpecificValues(seconds);
            return this.minutesExpressionBuilder;
        }

        public MinutesExpressionBuilder RangeOfSeconds(int from, int to)
        {
            this.secondsField.RangeOfValues(from, to);
            return this.minutesExpressionBuilder;
        }

        public MinutesExpressionBuilder AllSeconds()
        {
            this.secondsField.AllValues();
            return this.minutesExpressionBuilder;
        }

        public MinutesExpressionBuilder RunInSecondIncrements(int startingValue, int increment)
        {
            this.secondsField.RunInIncrements(startingValue, increment);
            return this.minutesExpressionBuilder;
        }

        public string BuildCronExpression() => $"{this.secondsField.CronExpression} {this.minutesExpressionBuilder.BuildCronExpression()}".Trim();

        public CronScheduleBuilder BuildCronSchedule()
        {
            var cronExpress = BuildCronExpression();
            return CronScheduleBuilder.CronSchedule(BuildCronExpression());
        }
    }

    internal enum CronDaysOfWeek
    {
        SUN = 1,
        MON = 2,
        TUE = 3,
        WED = 4,
        THU = 5,
        FRI = 6,
        SAT = 7
    }

    internal enum CronMonth
    {
        JAN = 1,
        FEB = 2,
        MAR = 3,
        APR = 4,
        MAY = 5,
        JUN = 6,
        JUL = 7,
        AUG = 8,
        SEP = 9,
        OCT = 10,
        NOV = 11,
        DEC = 12
    }

    internal class CronField
    {
        protected readonly List<char> allowedTokens;

        public string CronExpression { get; protected set; }

        public CronField()
        {
            this.IsMandatory = true;
            this.MinValue = 0;
            this.MaxValue = 59;
            this.CronExpression = "0";
            this.allowedTokens = new List<char> { ',', '-', '*', '/' };
        }

        public bool IsMandatory { get; protected set; }
        public int MaxValue { get; protected set; }
        public int MinValue { get; protected set; }

        public void RangeOfValues(int from, int to)
        {
            this.CronExpression = $"{from}-{to}";
        }

        public void RunInIncrements(int startingValue, int increment)
        {
            this.CronExpression = $"{startingValue}/{increment}";
        }

        public void RunEveryXMinutes(int interval)
        {
            this.CronExpression = $"*/{interval}";
        }

        public void AllValues()
        {
            this.CronExpression = "*";
        }

        public void SpecificValues(params int[] specificValues)
        {
            if (this.ValuesAreValid(specificValues))
            {
                this.CronExpression = string.Join(",", specificValues);
            }
            else
            {
                throw new ArgumentException("You provided invalid values for the cron expression!");
            }
        }

        public bool ValuesAreValid(params int[] values)
        {
            return !values.Any(v => v < this.MinValue || v > this.MaxValue);
        }
    }

    internal class DayOfMonthField : CronField
    {
        public DayOfMonthField() : base()
        {
            this.allowedTokens.AddRange(new char[] { '?', 'L', 'W' });
            this.MinValue = 1;
            this.MaxValue = 31;
        }

        public void NoSpecificValue()
        {
            this.CronExpression = "?";
        }

        public void WeekdaysOnly()
        {
            this.CronExpression = "W";
        }

        public void Last()
        {
            this.CronExpression = "L";
        }
    }

    internal class DayOfWeekField : CronField
    {
        public DayOfWeekField() : base()
        {
            this.allowedTokens.AddRange(new char[] { '?', 'L', '#' });
            this.MinValue = 1;
            this.MaxValue = 7;
        }

        public void NoSpecificValue()
        {
            this.CronExpression = "?";
        }

        public void SpecificValue(params CronDaysOfWeek[] daysOfWeek)
        {
            var intValues = daysOfWeek.Select(day => (int)day).ToArray();
            this.SpecificValues(intValues);
        }

        public void NthDayOfMonth(int dayOfWeek, int weekNumber)
        {
            this.CronExpression = $"{dayOfWeek}#{weekNumber}";
        }

        public void Last()
        {
            this.CronExpression = "L";
        }
    }

    internal class HoursField : CronField
    {
        public HoursField() : base()
        {
            this.MaxValue = 23;
        }
    }

    internal class MinutesField : CronField
    {
        public MinutesField() : base()
        {
        }
    }

    internal class MonthField : CronField
    {
        public MonthField() : base()
        {
            this.MinValue = 1;
            this.MaxValue = 12;
        }

        public void SpecificValue(params CronMonth[] months)
        {
            var intValues = months.Select(month => (int)month).ToArray();
            this.SpecificValues(intValues);
        }
    }

    internal class SecondsField : CronField
    {
        public SecondsField() : base()
        {
        }
    }

    internal class YearField : CronField
    {
        public YearField() : base()
        {
            this.IsMandatory = false;
            this.MinValue = 1970;
            this.MaxValue = 2099;
            this.CronExpression = "";
        }
    }
}
