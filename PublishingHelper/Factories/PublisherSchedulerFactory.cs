﻿using System.Threading;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;

namespace PublishingHelper.Factories
{
    internal class PublisherSchedulerFactory : StdSchedulerFactory
    {
        public override async Task<IScheduler> GetScheduler(CancellationToken cancellationToken = default(CancellationToken))
        {
            {
                var scheduler = await base.GetScheduler(cancellationToken);

                // Schedule RunScheduledJobs job
                //var runScheduledJobsCronSchedule = CronExpressionBuilder.NewExpression().SpecificSeconds(30).SpecificMinutes(0, 15, 30, 45).AllHours().AllDaysOfTheMonth().AllMonths().NoSpecificDaysOfTheWeek().AllYears().BuildCronSchedule();
                //var runScheduledJobsJob = JobBuilder.Create<RunScheduledJobsJob>().WithIdentity(typeof(RunScheduledJobsJob).Name).Build();
                //var runScheduledJobsTrigger = TriggerBuilder.Create().WithIdentity($"{typeof(RunScheduledJobsJob).Name}Trigger").StartNow().WithSchedule(runScheduledJobsCronSchedule).Build();
                //await scheduler.ScheduleJob(runScheduledJobsJob, runScheduledJobsTrigger);

                return scheduler;
            }
        }
    }
}
