﻿using System;
using System.IO;
using System.Threading;
using PublishingHelper.ChatBot.IoC;
using PublishingHelper.ChatBot.Services;
using PublishingHelper.Properties;
using StreamLabSharp;
using StreamLabSharp.Models;

namespace PublishingHelper.StreamLabs
{
    public class SpwnLabels : IDisposable
    {
        private readonly Action<string> loggerDelegate;
        private bool disposedValue;
        private Client streamLabsClient;

        public SpwnLabels(Action<string> loggerDelegate)
        {
            this.loggerDelegate = loggerDelegate;

            this.streamLabsClient = new Client();
            this.streamLabsClient.OnConnected += this.OnConnected;
            this.streamLabsClient.OnDisconnected += this.OnDisconnected;
            this.streamLabsClient.OnYouTubeSubscription += this.OnYouTubeSubscription;
            this.streamLabsClient.OnTwitchFollow += this.OnTwitchFollow;
            this.streamLabsClient.OnMixerFollow += this.OnMixerFollow;
            this.streamLabsClient.OnDonation += this.OnDonation;
            this.streamLabsClient.Connect(Settings.Default.StreamLabsApiKey);
        }

        private void OnDonation(object sender, StreamlabsEvent<DonationMessage> events)
        {
            var message = events.Message[0];
            var follower = message.From;
            var amount = message.FormattedAmount;

            WriteRecentTipper(follower, amount);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.streamLabsClient.Disconnect();
                    this.streamLabsClient.OnConnected -= this.OnConnected;
                    this.streamLabsClient.OnDisconnected -= this.OnDisconnected;
                    this.streamLabsClient.OnYouTubeSubscription -= this.OnYouTubeSubscription;
                    this.streamLabsClient.OnTwitchFollow -= this.OnTwitchFollow;
                    this.streamLabsClient.OnMixerFollow -= this.OnMixerFollow;
                    this.streamLabsClient.OnDonation -= this.OnDonation;
                    this.streamLabsClient = null;
                }

                disposedValue = true;
            }
        }

        private void OnConnected(object sender, bool events)
        {
            this.loggerDelegate("StreamLabels socket connected");
        }

        private void OnDisconnected(object sender, bool events)
        {
            this.loggerDelegate("StreamLabels socket disconnected");
        }

        private void OnMixerFollow(object sender, StreamlabsEvent<MixerFollowMessage> events)
        {
            var message = events.Message[0];
            var follower = message.Name;
            WriteRecentFollower(follower);
        }

        private void OnTwitchFollow(object sender, StreamlabsEvent<TwitchFollowMessage> events)
        {
            var message = events.Message[0];
            var follower = message.Name;
            WriteRecentFollower(follower);
        }

        private void OnYouTubeSubscription(object sender, StreamlabsEvent<YouTubeSubscriptionMessage> events)
        {
            var message = events.Message[0];
            var follower = message.Name;
            WriteRecentFollower(follower);
        }

        private void WriteRecentFollower(string follower)
        {
            this.loggerDelegate($"Writing {follower} as most recent follower overall");

            ServiceLocator.Current.Resolve<ISlobsService>().ShowNewFollowerAnimation();

            Thread.Sleep(300);
            File.WriteAllText(Settings.Default.SpwnLabelsFollowerPath, follower);

            Thread.Sleep(3000);
            ServiceLocator.Current.Resolve<ISlobsService>().HideNewFollowerAnimation();
        }

        private void WriteRecentTipper(string follower, string amount)
        {
            this.loggerDelegate($"Writing {follower} as most recent tipper for {amount}");

            ServiceLocator.Current.Resolve<ISlobsService>().ShowNewTipperAnimation();

            Thread.Sleep(300);
            File.WriteAllText(Settings.Default.SpwnLabelsTipperPath, $"{follower} - {amount}");

            Thread.Sleep(3000);
            ServiceLocator.Current.Resolve<ISlobsService>().HideNewTipperAnimation();
        }
    }
}
