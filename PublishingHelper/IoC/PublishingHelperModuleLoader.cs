﻿using Google.Apis.Util.Store;
using LightInject;
using PublishingHelper.ChatBot.IoC;
using PublishingHelper.Repository;
using PublishingHelper.Service;
using PublishingHelper.Youtube;

namespace PublishingHelper.IoC
{
    public class PublishingHelperModuleLoader : IModuleLoader
    {
        public void RegisterServices(IServiceContainer serviceContainer)
        {
            serviceContainer.Register<IYoutubeScheduledCommentRepository, YoutubeScheduledCommentRepository>();
            serviceContainer.Register<IYoutubeAuthenticationService, YoutubeAuthenticationService>();
            serviceContainer.Register<IYoutubeService, YoutubeService>();
            serviceContainer.Register<IDataStore, YoutubeCredentialDataStore>();
        }
    }
}
