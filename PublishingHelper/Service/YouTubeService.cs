﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.YouTube.v3.Data;
using PublishingHelper.ChatBot.IoC;
using PublishingHelper.Repository;
using PublishingHelper.Repository.Entities;
using PublishingHelper.Youtube;

namespace PublishingHelper.Service
{
    internal interface IYoutubeService
    {
        void CreateScheduleComment(string videoId, DateTime publishAt, string comment, string videoTitle);
        void DeleteScheduledComment(string videoId);
        Task<Video> GetVideoByIdAsync(string videoId, params string[] parts);
        Task PublishScheduledCommentAsync(string videoId, string comment);
        IEnumerable<YoutubeScheduledCommentEntity> ReadAllScheduledComments();
        IEnumerable<YoutubeScheduledCommentEntity> ReadAllScheduledCommentsNeedingPublished();
        YoutubeScheduledCommentEntity UpdateScheduledComment(YoutubeScheduledCommentEntity entity);
    }

    internal class YoutubeService : IYoutubeService
    {
        public YoutubeService()
        {
        }

        public void CreateScheduleComment(string videoId, DateTime publishAt, string comment, string videoTitle)
        {
            var entity = new YoutubeScheduledCommentEntity
            {
                Comment = comment,
                VideoId = videoId,
                PublishAt = publishAt,
                VideoTitle = videoTitle
            };

            var youtubeScheduledCommentRepository = ServiceLocator.Current.Resolve<IYoutubeScheduledCommentRepository>();
            youtubeScheduledCommentRepository.Create(entity);
        }

        public void DeleteScheduledComment(string videoId)
        {
            var youtubeScheduledCommentRepository = ServiceLocator.Current.Resolve<IYoutubeScheduledCommentRepository>();
            youtubeScheduledCommentRepository.Delete(videoId);
        }

        public async Task<Video> GetVideoByIdAsync(string videoId, params string[] parts)
        {
            var youtubeAuthenticationService = ServiceLocator.Current.Resolve<IYoutubeAuthenticationService>();
            var credential = await youtubeAuthenticationService.GetUserCredentialAsync();
            var youtubeClient = new YoutubeClient(credential);
            var video = youtubeClient.GetVideoById(videoId, parts);

            return video;
        }

        public async Task PublishScheduledCommentAsync(string videoId, string comment)
        {
            var youtubeAuthenticationService = ServiceLocator.Current.Resolve<IYoutubeAuthenticationService>();
            var credential = await youtubeAuthenticationService.GetUserCredentialAsync();
            var youtubeClient = new YoutubeClient(credential);
            var video = youtubeClient.GetVideoById(videoId);

            if (video != null)
            {
                var retries = 0;
                while (video.Status.PrivacyStatus != "public" && retries < 10)
                {
                    await Task.Delay(1000);
                    retries++;
                    video = youtubeClient.GetVideoById(videoId);
                }

                var commentResponse = youtubeClient.PostCommentToVideo(videoId, comment);
                youtubeClient.LikeComment(commentResponse.Snippet.TopLevelComment);
                youtubeClient.LikeVideo(videoId);
            }
        }

        public IEnumerable<YoutubeScheduledCommentEntity> ReadAllScheduledComments()
        {
            var youtubeScheduledCommentRepository = ServiceLocator.Current.Resolve<IYoutubeScheduledCommentRepository>();
            return youtubeScheduledCommentRepository.ReadAll();
        }

        public IEnumerable<YoutubeScheduledCommentEntity> ReadAllScheduledCommentsNeedingPublished()
        {
            return this.ReadAllScheduledComments().Where(e => e.PublishAt.AddMinutes(1) <= DateTime.Now);
        }

        public YoutubeScheduledCommentEntity UpdateScheduledComment(YoutubeScheduledCommentEntity entity)
        {
            var youtubeScheduledCommentRepository = ServiceLocator.Current.Resolve<IYoutubeScheduledCommentRepository>();
            return youtubeScheduledCommentRepository.Update(entity);
        }
    }
}
