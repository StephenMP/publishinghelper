﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PublishingHelper.ChatBot.SLOBS.Models
{
    internal class SlobsRpcResponse
    {
        [JsonProperty("jsonrpc")]
        public string Jsonrpc { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("result")]
        public SlobsResult Result { get; set; }
    }

    internal class SlobsRpcArrayResponse
    {
        [JsonProperty("jsonrpc")]
        public string Jsonrpc { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("result")]
        public IEnumerable<SlobsResult> Result { get; set; }
    }

    internal class SlobsResult
    {
        [JsonProperty("_type")]
        public string Type { get; set; }

        [JsonProperty("resourceId")]
        public string ResourceId { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("nodes")]
        public List<SlobsNode> Nodes { get; set; }

        [JsonProperty("sourceId")]
        public string SourceId { get; set; }

        [JsonProperty("type")]
        public string ResultType { get; set; }

        [JsonProperty("audio")]
        public bool Audio { get; set; }

        [JsonProperty("video")]
        public bool Video { get; set; }

        [JsonProperty("async")]
        public bool Async { get; set; }

        [JsonProperty("doNotDuplicate")]
        public bool DoNotDuplicate { get; set; }

        [JsonProperty("width")]
        public long Width { get; set; }

        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("muted")]
        public bool Muted { get; set; }
    }

    internal partial class SlobsNode
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("sceneNodeType")]
        public SceneNodeType SceneNodeType { get; set; }

        [JsonProperty("sceneId")]
        public string SceneId { get; set; }

        [JsonProperty("resourceId")]
        public string ResourceId { get; set; }

        [JsonProperty("parentId")]
        public string ParentId { get; set; }

        [JsonProperty("childrenIds")]
        public List<Guid> ChildrenIds { get; set; }

        [JsonProperty("sceneItemId")]
        public Guid? SceneItemId { get; set; }

        [JsonProperty("sourceId")]
        public string SourceId { get; set; }

        [JsonProperty("obsSceneItemId")]
        public long? ObsSceneItemId { get; set; }

        [JsonProperty("transform")]
        public Transform Transform { get; set; }

        [JsonProperty("visible")]
        public bool? Visible { get; set; }

        [JsonProperty("locked")]
        public bool? Locked { get; set; }
    }

    internal partial class Transform
    {
        [JsonProperty("position")]
        public Position Position { get; set; }

        [JsonProperty("scale")]
        public Position Scale { get; set; }

        [JsonProperty("crop")]
        public Crop Crop { get; set; }

        [JsonProperty("rotation")]
        public long Rotation { get; set; }
    }

    internal partial class Crop
    {
        [JsonProperty("top")]
        public long Top { get; set; }

        [JsonProperty("bottom")]
        public long Bottom { get; set; }

        [JsonProperty("left")]
        public long Left { get; set; }

        [JsonProperty("right")]
        public long Right { get; set; }
    }

    internal partial class Position
    {
        [JsonProperty("x")]
        public double X { get; set; }

        [JsonProperty("y")]
        public double Y { get; set; }
    }

    internal enum SceneNodeType { Folder, Item };

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                SceneNodeTypeConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class SceneNodeTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(SceneNodeType) || t == typeof(SceneNodeType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "folder":
                    return SceneNodeType.Folder;
                case "item":
                    return SceneNodeType.Item;
            }
            throw new Exception("Cannot unmarshal type SceneNodeType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (SceneNodeType)untypedValue;
            switch (value)
            {
                case SceneNodeType.Folder:
                    serializer.Serialize(writer, "folder");
                    return;
                case SceneNodeType.Item:
                    serializer.Serialize(writer, "item");
                    return;
            }
            throw new Exception("Cannot marshal type SceneNodeType");
        }

        public static readonly SceneNodeTypeConverter Singleton = new SceneNodeTypeConverter();
    }
}
