﻿using System.Collections.Generic;

namespace PublishingHelper.ChatBot.SLOBS.Models
{
    internal class SlobsScene
    {
        public string Type { get; set; }
        public string ResourceId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public List<SlobsNode> Nodes { get; set; }
    }
}
