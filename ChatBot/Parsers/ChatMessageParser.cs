﻿using System.Text.RegularExpressions;
using PublishingHelper.ChatBot.Models;

namespace PublishingHelper.ChatBot.Parsers
{
    internal interface IChatMessageParser
    {
        ChatMessage ParseIncomingMessage(string user, string message);
    }

    internal class ChatMessageParser : IChatMessageParser
    {
        public ChatMessage ParseIncomingMessage(string user, string originalMessage)
        {
            var cleanedMessage = Regex.Replace(originalMessage, @"\[.*\]", "").Trim();
            var result = new ChatMessage
            {
                User = new ChatUser { Username = user },
                Message = cleanedMessage
            };

            // First, parse out the user if needed since the message could be a relayed message
            if (originalMessage.StartsWith("["))
            {
                result.User.Username = Regex.Match(originalMessage, @"(?<=\[).+?(?=\])").Value;
                result.Message = Regex.Replace(originalMessage, @"\[.*\]", "").Trim();
            }

            // Now, let's see if we have a command in the message
            if (cleanedMessage.StartsWith("!"))
            {
                var chatCommand = new ChatCommand
                {
                    Command = cleanedMessage
                };

                // If the message has a space, we know there were arguments provided
                if (cleanedMessage.Contains(" "))
                {
                    chatCommand.Command = cleanedMessage.Substring(0, cleanedMessage.IndexOf(" "));
                    chatCommand.Arguments = cleanedMessage.Substring(chatCommand.Command.Length + 1);
                }

                result.ChatCommand = chatCommand;
            }

            return result;
        }
    }
}
