﻿using AutoMapper;
using LightInject;
using PublishingHelper.ChatBot.Parsers;
using PublishingHelper.ChatBot.Pipes;
using PublishingHelper.ChatBot.Services;
using PublishingHelper.ChatBot.SLOBS.Models;
using PublishingHelper.Repository.Entities;
using SteamStoreQuery;
using TwitchLib.Client;
using TwitchLib.Client.Interfaces;

namespace PublishingHelper.ChatBot.IoC
{
    internal class ChatBotModuleLoader : IModuleLoader
    {
        public void RegisterServices(IServiceContainer serviceContainer)
        {
            serviceContainer.RegisterInstance<ITwitchClient>(new TwitchClient());
            serviceContainer.RegisterInstance(BuildMapper());
            serviceContainer.Register<ISlobsPipeClient, SlobsPipeClient>();
            serviceContainer.Register<ISlobsService, SlobsService>();
            serviceContainer.Register<IMusicService, MusicService>();
            serviceContainer.Register<IChatCommandService, ChatCommandService>();
            serviceContainer.Register<IChatMessageParser, ChatMessageParser>();
        }

        private IMapper BuildMapper()
        {
            var mapper = new MapperConfiguration(config =>
            {
                config.CreateMap<Listing, SuggestedGameEntity>();
                config.CreateMap<SlobsResult, SlobsSource>();
                config.CreateMap<SlobsResult, SlobsScene>();
            }).CreateMapper();

            return mapper;
        }
    }
}
