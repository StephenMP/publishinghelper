﻿using System;
using System.IO;
using System.Linq;
using PublishingHelper.ChatBot.Properties;

namespace PublishingHelper.ChatBot.Helpers
{
    internal class ChatModeratorHelper
    {
        private static ChatModeratorHelper current;
        private readonly string[] mods;

        public static ChatModeratorHelper Current
        {
            get
            {
                current = current ?? new ChatModeratorHelper();
                return current;
            }
        }

        private ChatModeratorHelper()
        {
            this.mods = File.ReadAllLines(Settings.Default.ModFilePath);
        }

        public bool UserIsAModerator(string username)
        {
            if (!string.IsNullOrWhiteSpace(username))
            {
                return this.mods.Contains(username.ToLower());
            }

            return false;
        }
    }
}
