﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Text.RegularExpressions;
using AutoMapper;
using PublishingHelper.ChatBot.IoC;
using PublishingHelper.ChatBot.Models;
using PublishingHelper.Repository;
using PublishingHelper.Repository.Entities;
using SteamStoreQuery;
using TwitchLib.Client.Interfaces;

namespace PublishingHelper.ChatBot.Services
{
    internal interface IChatCommandService : IDisposable
    {
        void InvokeCommand(ChatMessage chatMessage);
    }

    internal class ChatCommandService : IChatCommandService
    {
        private readonly IMapper mapper;
        private readonly IMusicService musicService;
        private readonly ISlobsService slobsService;
        private readonly ITwitchClient twitchClient;
        private bool disposedValue;

        public ChatCommandService()
        {
            this.twitchClient = ServiceLocator.Current.Resolve<ITwitchClient>();
            this.slobsService = ServiceLocator.Current.Resolve<ISlobsService>();
            this.musicService = ServiceLocator.Current.Resolve<IMusicService>();
            this.mapper = ServiceLocator.Current.Resolve<IMapper>();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }





        public void InvokeCommand(ChatMessage chatMessage)
        {
            if (CommandIsReady(chatMessage.ChatCommand.Command) || chatMessage.User.IsAModerator)
            {
                if (this.slobsService.IsAnimationCommand(chatMessage.ChatCommand.Command))
                {
                    this.slobsService.PlayAnimation(chatMessage.ChatCommand.Command);
                }

                else
                {
                    switch (chatMessage.ChatCommand.Command)
                    {
                        case "!live":
                            this.slobsService.SwitchToLiveScene();
                            break;

                        case "!music":

                            if (chatMessage.User.IsAModerator)
                            {
                                this.musicService.PlayPauseMusic();
                            }
                            break;

                        case "!enter":
                            ChatBotClient.EnterUserIntoGiveaway(chatMessage.User.Username);
                            break;

                        case "!entries":
                            var entryLines = File.ReadAllLines(@"C:\Users\Stephen\Desktop\PublishingHelper\giveaway.txt");
                            var entries = entryLines.Count(e => e == chatMessage.User.Username.ToLower());
                            this.twitchClient.SendMessage("mrspwn", $"Yo @{chatMessage.User.Username}, you've got {entries} entries!");
                            break;

                        case "!nextsong":
                            this.musicService.NextSong();
                            SetCommandOnCooldown(chatMessage.User.Username, chatMessage.ChatCommand.Command, 300);
                            break;

                        case "!suggestgame":
                            var suggestedGameRepository = new SuggestedGameRepository();
                            if (!suggestedGameRepository.Contains(chatMessage.ChatCommand.Arguments))
                            {
                                var suggestedGameEntity = new SuggestedGameEntity { SuggestedBy = chatMessage.User.Username, Name = chatMessage.ChatCommand.Arguments };

                                var listings = Query.Search(chatMessage.ChatCommand.Arguments);
                                if (listings != null && listings.Count > 0)
                                {
                                    var listing = listings.FirstOrDefault(sg => Regex.Replace(sg.Name, "[^a-zA-Z0-9 -]", "").ToLower() == chatMessage.ChatCommand.Arguments.ToLower());
                                    if (listing != null)
                                    {
                                        suggestedGameEntity = Mapper.Map<SuggestedGameEntity>(listing);
                                        suggestedGameEntity.SuggestedBy = chatMessage.User.Username;
                                    }
                                }

                                suggestedGameRepository.Create(suggestedGameEntity);
                            }

                            break;
                    }
                }
            }
        }



        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.slobsService.Dispose();
                }

                disposedValue = true;
            }
        }

        private static bool CommandIsReady(string command)
        {
            return !MemoryCache.Default.Contains(command);
        }

        private static void SetCommandOnCooldown(string user, string command, int timeInSeconds)
        {
            if (user.ToLower() != "mrspwn")
            {
                MemoryCache.Default.Add(command, command, DateTime.Now.AddSeconds(timeInSeconds));
            }
        }
    }
}
