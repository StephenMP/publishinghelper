﻿using System;
using System.Runtime.InteropServices;

namespace PublishingHelper.ChatBot.Services
{
    internal interface IMusicService
    {
        void NextSong();

        void PlayPauseMusic();
    }

    internal class MusicService : IMusicService
    {
        private const int KEYEVENT_KEYDOWN = 0x0001;

        //Key down flag
        private const int KEYEVENT_KEYUP = 0x0002;

        private const int VK_MEDIA_NEXT_TRACK = 0xB0;

        private const int VK_MEDIA_PLAY_PAUSE = 0xB3;

        private const int VK_MEDIA_PREV_TRACK = 0xB1;

        public void NextSong()
        {
            keybd_event(VK_MEDIA_NEXT_TRACK, 0, KEYEVENT_KEYDOWN, IntPtr.Zero);
            keybd_event(VK_MEDIA_NEXT_TRACK, 0, KEYEVENT_KEYUP, IntPtr.Zero);
        }

        public void PlayPauseMusic()
        {
            keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENT_KEYDOWN, IntPtr.Zero);
            keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENT_KEYUP, IntPtr.Zero);
        }

        [DllImport("user32.dll", SetLastError = true)]
        private static extern void keybd_event(byte virtualKey, byte scanCode, uint flags, IntPtr extraInfo);

        //Key up flag
    }
}
