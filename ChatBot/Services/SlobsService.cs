﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using AutoMapper;
using Newtonsoft.Json;
using PublishingHelper.ChatBot.Factories;
using PublishingHelper.ChatBot.IoC;
using PublishingHelper.ChatBot.Pipes;
using PublishingHelper.ChatBot.SLOBS.Models;

namespace PublishingHelper.ChatBot.Services
{
    public interface ISlobsService : IDisposable
    {
        void HideNewFollowerAnimation();

        void HideNewTipperAnimation();

        bool IsAnimationCommand(string command);

        void PlayAnimation(string command);

        void ShowNewFollowerAnimation();

        void ShowNewTipperAnimation();

        void SwitchToLiveScene();
    }

    public class SlobsService : ISlobsService
    {
        private readonly IDictionary<string, AnimationConfiguration> animationConfigurationsByCommand;
        private readonly ISlobsPipeClient slobsClient;
        private bool disposedValue;

        public SlobsService()
        {
            this.slobsClient = ServiceLocator.Current.Resolve<ISlobsPipeClient>();
            this.slobsClient.Connect();

            this.animationConfigurationsByCommand = new Dictionary<string, AnimationConfiguration>();
            var animationsConfigPath = @"C:\Users\Stephen\Desktop\PublishingHelper\animations.json";
            var animationConfigsJson = File.ReadAllText(animationsConfigPath);
            var animationConfigs = JsonConvert.DeserializeObject<List<AnimationConfiguration>>(animationConfigsJson);
            foreach (var config in animationConfigs)
            {
                this.animationConfigurationsByCommand.Add(config.AnimationCommand, config);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void HideNewFollowerAnimation()
        {
            this.ToggleAnimation("Video - Follower Change", false);
        }

        public void HideNewTipperAnimation()
        {
            this.ToggleAnimation("Video - Tipper Change", false);
        }

        public bool IsAnimationCommand(string command)
        {
            return this.animationConfigurationsByCommand.ContainsKey(command);
        }

        public void PlayAnimation(string command)
        {
            using (var slobsReader = new StreamReader(this.slobsClient.Pipe))
            {
                using (var slobsWriter = new StreamWriter(this.slobsClient.Pipe))
                {
                    if (this.animationConfigurationsByCommand.ContainsKey(command))
                    {
                        var activeScene = GetActiveScene(slobsReader, slobsWriter);
                        if (activeScene != null)
                        {
                            var activeSources = GetActiveSceneItemSources(activeScene, slobsReader, slobsWriter);
                            if (activeSources != null)
                            {
                                var animationConfig = this.animationConfigurationsByCommand[command];
                                var showCommandBuilder = new StringBuilder();
                                var hideCommandBuilder = new StringBuilder();

                                foreach (var config in animationConfig.AnimationResources)
                                {
                                    var animationSource = activeSources.FirstOrDefault(source => source.Name == config);
                                    if (animationSource != null)
                                    {
                                        var sceneItem = activeScene.Nodes.FirstOrDefault(node => node.SourceId == animationSource.SourceId);
                                        if (sceneItem != null)
                                        {
                                            showCommandBuilder.AppendLine(SlobsCommandFactory.SetSceneItemVisibility(sceneItem.ResourceId, true));
                                            hideCommandBuilder.AppendLine(SlobsCommandFactory.SetSceneItemVisibility(sceneItem.ResourceId, false));
                                        }
                                    }
                                }

                                slobsWriter.Write(showCommandBuilder.ToString());
                                slobsWriter.Flush();

                                Thread.Sleep(animationConfig.AnimationDelay);

                                slobsWriter.Write(hideCommandBuilder.ToString());
                                slobsWriter.Flush();
                            }
                        }
                    }
                }
            }
        }

        public void ShowNewFollowerAnimation()
        {
            this.ToggleAnimation("Video - Follower Change", true);
        }

        public void ShowNewTipperAnimation()
        {
            this.ToggleAnimation("Video - Tipper Change", true);
        }

        public void SwitchToLiveScene()
        {
            using (var slobsReader = new StreamReader(this.slobsClient.Pipe))
            {
                using (var slobsWriter = new StreamWriter(this.slobsClient.Pipe))
                {
                    var activeSceneResponse = GetActiveScene(slobsReader, slobsWriter);
                    if (activeSceneResponse.Name == "Intermission")
                    {
                        var scenes = GetScenes(slobsReader, slobsWriter);
                        var liveScene = scenes.FirstOrDefault(scene => scene.Name == "Live");

                        if (liveScene != null)
                        {
                            slobsWriter.WriteLine(SlobsCommandFactory.SwitchScenes(liveScene.Id));
                            slobsWriter.Flush();
                        }
                    }
                }
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.slobsClient.Dispose();
                }

                disposedValue = true;
            }
        }

        private static SlobsScene GetActiveScene(StreamReader slobsReader, StreamWriter slobsWriter)
        {
            var mapper = ServiceLocator.Current.Resolve<IMapper>();
            slobsWriter.WriteLine(SlobsCommandFactory.GetActiveScene());
            slobsWriter.Flush();

            var activeSceneResponseJson = slobsReader.ReadLine();
            var activeSceneResponse = JsonConvert.DeserializeObject<SlobsRpcResponse>(activeSceneResponseJson);
            var activeScene = mapper.Map<SlobsScene>(activeSceneResponse.Result);

            return activeScene;
        }

        private static IEnumerable<SlobsSource> GetActiveSceneItemSources(SlobsScene activeScene, StreamReader slobsReader, StreamWriter slobsWriter)
        {
            var mapper = ServiceLocator.Current.Resolve<IMapper>();
            var itemNodes = activeScene.Nodes.Where(node => node.SceneNodeType == SceneNodeType.Item);
            var activeSources = new List<SlobsSource>();
            var jsonResponse = string.Empty;

            SlobsRpcResponse rpcResponse;
            SlobsSource slobsSource;

            foreach (var node in itemNodes)
            {
                slobsWriter.WriteLine(SlobsCommandFactory.GetSource(node.SourceId));
                slobsWriter.Flush();

                jsonResponse = slobsReader.ReadLine();
                rpcResponse = JsonConvert.DeserializeObject<SlobsRpcResponse>(jsonResponse);
                slobsSource = mapper.Map<SlobsSource>(rpcResponse.Result);

                activeSources.Add(slobsSource);
            }

            return activeSources;
        }

        private static IEnumerable<SlobsScene> GetScenes(StreamReader slobsReader, StreamWriter slobsWriter)
        {
            var mapper = ServiceLocator.Current.Resolve<IMapper>();
            var command = new
            {
                jsonrpc = "2.0",
                id = 10,
                method = "scenes",
                @params = new
                {
                    resource = "ScenesService"
                }
            };

            var jsonCommand = JsonConvert.SerializeObject(command);

            slobsWriter.WriteLine(jsonCommand);
            slobsWriter.Flush();

            var jsonReponse = slobsReader.ReadLine();
            var response = JsonConvert.DeserializeObject<SlobsRpcArrayResponse>(jsonReponse);
            var scenes = mapper.Map<List<SlobsScene>>(response.Result);

            return scenes;
        }

        private void ToggleAnimation(string animationName, bool show)
        {
            using (var slobsReader = new StreamReader(this.slobsClient.Pipe))
            {
                using (var slobsWriter = new StreamWriter(this.slobsClient.Pipe))
                {
                    var activeScene = GetActiveScene(slobsReader, slobsWriter);
                    if (activeScene != null)
                    {
                        var activeSources = GetActiveSceneItemSources(activeScene, slobsReader, slobsWriter);
                        if (activeSources != null)
                        {
                            var animationSource = activeSources.FirstOrDefault(source => source.Name == animationName);
                            if (animationSource != null)
                            {
                                var sceneItem = activeScene.Nodes.FirstOrDefault(node => node.SourceId == animationSource.SourceId);
                                if (sceneItem != null)
                                {
                                    slobsWriter.Write(SlobsCommandFactory.SetSceneItemVisibility(sceneItem.ResourceId, show));
                                    slobsWriter.Flush();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    internal class AnimationConfiguration
    {
        public string AnimationCommand { get; set; }
        public int AnimationDelay { get; set; }
        public IEnumerable<string> AnimationResources { get; set; }
    }
}
