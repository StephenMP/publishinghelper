﻿using System.Collections.Generic;

namespace PublishingHelper.ChatBot.Models
{
    internal class ChatCommand
    {
        public string Arguments { get; set; }
        public IEnumerable<string> ArgumentSegments => this.Arguments.Split(' ');
        public string Command { get; set; }
    }
}
