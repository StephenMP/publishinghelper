﻿namespace PublishingHelper.ChatBot.Models
{
    internal class ChatMessage
    {
        public ChatCommand ChatCommand { get; set; }
        public bool IsACommand => !string.IsNullOrWhiteSpace(this.ChatCommand.Command);
        public ChatUser User { get; set; }
        public string Message { get; set; }

        public ChatMessage()
        {
            this.User = new ChatUser();
            this.ChatCommand = new ChatCommand();
        }
    }
}
