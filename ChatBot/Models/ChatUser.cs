﻿using PublishingHelper.ChatBot.Helpers;

namespace PublishingHelper.ChatBot.Models
{
    internal class ChatUser
    {
        public bool IsAModerator => ChatModeratorHelper.Current.UserIsAModerator(this.Username);
        public string Username { get; set; }
    }
}
