﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using PublishingHelper.ChatBot.IoC;
using PublishingHelper.ChatBot.Parsers;
using PublishingHelper.ChatBot.Services;
using TwitchLib.Client.Events;
using TwitchLib.Client.Interfaces;
using TwitchLib.Client.Models;

namespace PublishingHelper.ChatBot
{
    public class ChatBotClient : IDisposable
    {
        private const string GiveawayPath = @"C:\Users\Stephen\Desktop\PublishingHelper\giveaway.txt";
        private readonly Action<string> Log;
        private bool disposedValue;

        public ChatBotClient(string twitchOauthToken, Action<string> Log)
        {
            this.Log = Log;

            var twitchClient = ServiceLocator.Current.Resolve<ITwitchClient>();

            if (!twitchClient.IsInitialized)
            {
                twitchClient.Initialize(new ConnectionCredentials("mrspwn", twitchOauthToken), "mrspwn");
            }

            twitchClient.OnJoinedChannel += OnJoinedChannel;
            twitchClient.OnMessageReceived += OnMessageReceived;
            twitchClient.OnConnected += OnConnected;
            twitchClient.OnDisconnected += OnDisconnected;
            twitchClient.Connect();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    var twitchClient = ServiceLocator.Current.Resolve<ITwitchClient>();
                    twitchClient.OnJoinedChannel -= OnJoinedChannel;
                    twitchClient.OnMessageReceived -= OnMessageReceived;
                    twitchClient.OnConnected -= OnConnected;
                    twitchClient.OnDisconnected -= OnDisconnected;
                    twitchClient.Disconnect();
                    twitchClient = null;
                }

                disposedValue = true;
            }
        }

        private void OnConnected(object sender, OnConnectedArgs e)
        {
            this.Log($"Chat bot connected to {e.AutoJoinChannel} acccount");
        }

        private void OnDisconnected(object sender, OnDisconnectedArgs e)
        {
            this.Log("Chat bot disconnected");
        }

        private void OnJoinedChannel(object sender, OnJoinedChannelArgs e)
        {
            this.Log($"Chat bot connected to {e.Channel} chat");
        }

        private static void ReEnterUserIntoGiveaway(string username)
        {
            if (UserHasAlreadyEnteredTheGiveaway(username))
            {
                EnterUserIntoGiveaway(username);
            }
        }

        internal static void EnterUserIntoGiveaway(string username)
        {
            if (UserCanEnterGiveaway(username))
            {
                File.AppendAllText(GiveawayPath, $"{username.ToLower()}\n");
                MemoryCache.Default.Add($"giveaway-{username.ToLower()}", username, DateTime.Now.AddMinutes(10));
            }
        }

        private static bool UserCanEnterGiveaway(string username)
        {
            var giveawayStartDate = new DateTime(2018, 9, 14);
            return DateTime.Now >= giveawayStartDate && !MemoryCache.Default.Contains($"giveaway-{username.ToLower()}");
        }

        private static bool UserHasAlreadyEnteredTheGiveaway(string username)
        {
            var currentEntries = File.ReadAllLines(GiveawayPath);
            return currentEntries.Any(e => e == username.ToLower());
        }

        private void OnCommandReceived(object sender, OnChatCommandReceivedArgs e)
        {

        }

        private void OnMessageReceived(object sender, OnMessageReceivedArgs e)
        {
            var chatMessageParser = ServiceLocator.Current.Resolve<IChatMessageParser>();
            var chatMessage = chatMessageParser.ParseIncomingMessage(e.ChatMessage.Username, e.ChatMessage.Message);

            ReEnterUserIntoGiveaway(chatMessage.User.Username);

            if (chatMessage.IsACommand)
            {
                using (var chatCommandService = ServiceLocator.Current.Resolve<IChatCommandService>())
                {
                    chatCommandService.InvokeCommand(chatMessage);
                }
            }
        }
    }
}
