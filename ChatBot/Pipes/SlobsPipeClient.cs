﻿using System;
using System.IO.Pipes;

namespace PublishingHelper.ChatBot.Pipes
{
    internal interface ISlobsPipeClient : IDisposable
    {
        void Connect();
        NamedPipeClientStream Pipe { get; }
    }

    internal class SlobsPipeClient : ISlobsPipeClient
    {
        public NamedPipeClientStream Pipe { get; }

        public SlobsPipeClient()
        {
            this.Pipe = new NamedPipeClientStream("slobs");
        }

        public void Connect()
        {
            this.Pipe.Connect();
        }

        #region IDisposable Support
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.Pipe.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
