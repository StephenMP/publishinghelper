﻿using Newtonsoft.Json;

namespace PublishingHelper.ChatBot.Factories
{
    internal class SlobsCommandFactory
    {
        public const string IntermissionScene = "Scene[\"scene_4def1ac8-213c-4005-bdfe-bcf60c91a4d9\"]";
        public const string LiveScene = "Scene[\"scene_0111a961-1ae9-4c74-b09e-506a6b4a59ef\"]";

        public static string GetSource(string sourceId)
        {
            var command = new
            {
                jsonrpc = "2.0",
                id = 10,
                method = "getSource",
                @params = new
                {
                    resource = "SourcesService",
                    args = new string[] { sourceId }
                }
            };

            var jsonCommand = JsonConvert.SerializeObject(command);

            return jsonCommand;
        }

        public static string GetActiveScene()
        {
            var command = new
            {
                jsonrpc = "2.0",
                id = 8,
                method = "activeScene",
                @params = new
                {
                    resource = "ScenesService"
                }
            };

            var commandJson = JsonConvert.SerializeObject(command);
            return commandJson;
        }

        public static string SwitchScenes(string sceneId)
        {
            var command = new
            {
                jsonrpc = "2.0",
                id = 8,
                method = "makeSceneActive",
                @params = new
                {
                    resource = "ScenesService",
                    args = new string[] { sceneId }
                }
            };

            var commandJson = JsonConvert.SerializeObject(command);
            return commandJson;
        }

        internal static string SetSceneItemVisibility(string sceneItemResourceId, bool show)
        {
            var command = new
            {
                jsonrpc = "2.0",
                id = 10,
                method = "setVisibility",
                @params = new
                {
                    resource = sceneItemResourceId,
                    args = new bool[] { show }
                }
            };

            var jsonCommand = JsonConvert.SerializeObject(command);
            return jsonCommand;
        }
    }
}
