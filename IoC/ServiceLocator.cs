﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using LightInject;

namespace PublishingHelper.ChatBot.IoC
{
    public interface IServiceLocator : IDisposable
    {
        T Resolve<T>();
    }

    public class ServiceLocator
    {
        public static IServiceLocator Current { get; set; }

        static ServiceLocator()
        {
            Current = new DefaultServiceLocator();
        }

        private class DefaultServiceLocator : IServiceLocator
        {
            private readonly IServiceContainer serviceContainer;

            private bool disposedValue;

            public DefaultServiceLocator()
            {
                this.serviceContainer = new ServiceContainer();
                this.RegisterServices();
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            public T Resolve<T>()
            {
                return this.serviceContainer.GetInstance<T>();
            }

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        this.serviceContainer.Dispose();
                    }

                    disposedValue = true;
                }
            }

            private IEnumerable<IModuleLoader> GetModuleLoaders(Assembly loadedAssembly)
            {
                var moduleLoaders = loadedAssembly.GetTypes()
                                                  .Where(t => t.GetInterfaces().Contains(typeof(IModuleLoader)) && t.GetConstructor(Type.EmptyTypes) != null)
                                                  .Select(t => Activator.CreateInstance(t) as IModuleLoader);

                return moduleLoaders;
            }

            private void RegisterServices()
            {
                var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies().Where(assembly => assembly.FullName.Contains("PublishingHelper"));

                foreach (var loadedAssembly in loadedAssemblies)
                {
                    var moduleLoaders = GetModuleLoaders(loadedAssembly);
                    foreach (var moduleLoader in moduleLoaders)
                    {
                        moduleLoader.RegisterServices(this.serviceContainer);
                    }
                }
            }
        }
    }
}
