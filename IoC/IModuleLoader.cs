﻿using LightInject;

namespace PublishingHelper.ChatBot.IoC
{
    public interface IModuleLoader
    {
        void RegisterServices(IServiceContainer serviceContainer);
    }
}
