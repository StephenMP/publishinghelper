﻿using Xunit;

namespace PublishingHelper.Tests.ChatBot.Factories
{
    public class SlobsCommandFactoryFeatures
    {
        private SlobsCommandFactorySteps steps;

        public SlobsCommandFactoryFeatures()
        {
            this.steps = new SlobsCommandFactorySteps();
        }

        [Fact]
        public void CanBuildGetActiveSceneCommand()
        {
            this.steps.WhenICallGetActiveSceneCommand();

            this.steps.ThenIShouldReceiveJsonIrcCommand();
            this.steps.ThenTheCommandShouldBeAGetActiveSceneCommand();
        }
    }
}
