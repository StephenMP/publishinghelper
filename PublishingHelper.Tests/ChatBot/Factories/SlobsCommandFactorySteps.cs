﻿using PublishingHelper.ChatBot.Factories;
using Xunit;

namespace PublishingHelper.Tests.ChatBot.Factories
{
    internal class SlobsCommandFactorySteps
    {
        private string jsonIrcCommand;

        public SlobsCommandFactorySteps()
        {
        }

        internal void WhenICallGetActiveSceneCommand()
        {
            this.jsonIrcCommand = SlobsCommandFactory.GetActiveScene();
        }

        internal void ThenIShouldReceiveJsonIrcCommand()
        {
            Assert.NotNull(this.jsonIrcCommand);
            Assert.NotEmpty(this.jsonIrcCommand);
        }

        internal void ThenTheCommandShouldBeAGetActiveSceneCommand()
        {
            Assert.Contains("activeScene", this.jsonIrcCommand);
            Assert.Contains("resource", this.jsonIrcCommand);
            Assert.Contains("ScenesService", this.jsonIrcCommand);
        }
    }
}