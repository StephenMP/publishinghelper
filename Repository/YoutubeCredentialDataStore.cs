﻿using System.Threading.Tasks;
using Google.Apis.Util.Store;
using LiteDB;
using Newtonsoft.Json;
using PublishingHelper.Repository.Properties;

namespace PublishingHelper.Youtube
{
    public class YoutubeCredentialDataStore : IDataStore
    {
        public Task ClearAsync()
        {
            using (var database = Database())
            {
                var collection = database.DropCollection("YoutubeCredential");
                return Task.CompletedTask;
            }
        }

        public Task DeleteAsync<T>(string key)
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                collection.Delete(c => c.Key == key);
                return Task.CompletedTask;
            }
        }

        public Task<T> GetAsync<T>(string key)
        {
            using (var database = Database())
            {
                var completionSource = new TaskCompletionSource<T>();
                var collection = Collection(database);
                var youtubeCredential = collection.FindOne(c => c.Key == key);

                if (youtubeCredential == null || youtubeCredential.Value == null)
                {
                    completionSource.SetResult(default(T));
                }
                else
                {
                    var token = JsonConvert.DeserializeObject<T>(youtubeCredential.Value.ToString());
                    completionSource.SetResult(token);
                }

                return completionSource.Task;
            }
        }

        public Task StoreAsync<T>(string key, T value)
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                var jsonValue = JsonConvert.SerializeObject(value);

                var youtubeCredential = new YoutubeCredential
                {
                    Key = key,
                    Value = jsonValue
                };

                collection.Upsert(youtubeCredential);
                collection.EnsureIndex(c => c.Key);
                return Task.CompletedTask;
            }
        }

        private LiteCollection<YoutubeCredential> Collection(LiteDatabase database) => database.GetCollection<YoutubeCredential>("YoutubeCredential");

        private LiteDatabase Database() => new LiteDatabase(Settings.Default.DbLocation);
    }
}
