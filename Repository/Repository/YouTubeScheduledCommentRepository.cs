﻿using PublishingHelper.Repository.Entities;

namespace PublishingHelper.Repository
{
    public interface IYoutubeScheduledCommentRepository : IRepository<YoutubeScheduledCommentEntity>
    {
        void Delete(string videoId);
        YoutubeScheduledCommentEntity Read(string videoId);
    }

    public class YoutubeScheduledCommentRepository : Repository<YoutubeScheduledCommentEntity>, IYoutubeScheduledCommentRepository
    {
        public YoutubeScheduledCommentRepository() : base("YoutubeScheduledComment")
        {
        }

        public YoutubeScheduledCommentEntity Create(YoutubeScheduledCommentEntity entity)
        {
            return base.Create(entity, "VideoId");
        }

        public void Delete(string videoId)
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                collection.Delete(e => e.VideoId == videoId);
            }
        }

        public YoutubeScheduledCommentEntity Read(string videoId)
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                return collection.FindOne(e => e.VideoId == videoId);
            }
        }
    }
}
