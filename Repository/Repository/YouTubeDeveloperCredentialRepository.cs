﻿using System.IO;
using LiteDB;
using PublishingHelper.Repository.Properties;

namespace PublishingHelper.Repository.Repository
{
    public class YouTubeDeveloperCredentialRepository
    {
        public Stream GetDeveloperClientCredentials()
        {
            using (var database = new LiteDatabase(Settings.Default.DbLocation))
            {
                return database.FileStorage.OpenRead("$/youtube/client_id.json");
            }
        }
    }
}
