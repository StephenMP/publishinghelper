﻿using System.Collections.Generic;

namespace PublishingHelper.Repository
{
    public interface IRepository<TEntity>
    {
        TEntity Create(TEntity entity, params string[] indexes);

        void Delete(int id);

        void Delete(TEntity entity);

        TEntity Read(int id);

        IEnumerable<TEntity> ReadAll();

        TEntity Update(TEntity entity);
    }
}
