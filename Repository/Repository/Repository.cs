﻿using System.Collections.Generic;
using LiteDB;
using PublishingHelper.Repository.Entities;
using PublishingHelper.Repository.Properties;

namespace PublishingHelper.Repository
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly string collectionName;

        protected Repository(string collectionName)
        {
            this.collectionName = collectionName;
        }

        public virtual TEntity Create(TEntity entity, params string[] indexes)
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                collection.Insert(entity);

                foreach (var index in indexes)
                {
                    collection.EnsureIndex(index);
                }
            }

            return entity;
        }

        public virtual void Delete(int id)
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                collection.Delete(e => e.Id == id);
            }
        }

        public virtual void Delete(TEntity entity)
        {
            this.Delete(entity.Id);
        }

        public virtual TEntity Read(int id)
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                return collection.FindOne(e => e.Id == id);
            }
        }

        public virtual IEnumerable<TEntity> ReadAll()
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                return collection.FindAll();
            }
        }

        public virtual TEntity Update(TEntity entity)
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                collection.Update(entity);
            }

            return entity;
        }

        protected LiteCollection<TEntity> Collection(LiteDatabase database) => database.GetCollection<TEntity>(this.collectionName);

        protected LiteDatabase Database() => new LiteDatabase(Settings.Default.DbLocation);
    }
}
