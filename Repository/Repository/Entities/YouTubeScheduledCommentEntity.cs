﻿using System;

namespace PublishingHelper.Repository.Entities
{
    public class YoutubeScheduledCommentEntity : Entity
    {
        public string Comment { get; set; }
        public DateTime PublishAt { get; set; }
        public string VideoId { get; set; }
        public string VideoTitle { get; set; }
    }
}
