﻿using System.Text;

namespace PublishingHelper.Repository.Entities
{
    public class SuggestedGameEntity : Entity
    {
        public int AppId { get; set; }
        public string ImageLink { get; set; }
        public string Name { get; set; }
        public double? PriceUSD { get; set; }
        public string StoreLink { get; set; }
        public string SuggestedBy { get; set; }

        public override string ToString()
        {
            return new StringBuilder()
                        .AppendLine($"Id: {Id}")
                        .AppendLine($"Suggested By: {SuggestedBy}")
                        .AppendLine($"Game Name: {Name}")
                        .AppendLine($"Game Price: {PriceUSD}")
                        .AppendLine($"Store Link: {StoreLink}")
                        .ToString();
        }
    }
}
