﻿using System.Linq;
using PublishingHelper.Repository.Entities;

namespace PublishingHelper.Repository
{
    public class SuggestedGameRepository : Repository<SuggestedGameEntity>
    {
        public SuggestedGameRepository() : base(nameof(SuggestedGameRepository).Replace("Repository", ""))
        {
        }

        public bool Contains(string gameName)
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                var results = collection.Find(sg => sg.Name.ToLower() == gameName.ToLower());
                return results != null && results.Count() > 0;
            }
        }
    }
}
