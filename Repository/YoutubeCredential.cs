﻿using PublishingHelper.Repository.Entities;

namespace PublishingHelper.Youtube
{
    public class YoutubeCredential : Entity
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
